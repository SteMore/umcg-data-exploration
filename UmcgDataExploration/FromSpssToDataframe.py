import pyreadstat
import pandas as pd

pd.set_option('display.max_columns', None)
pd.set_option('display.width', 1600)


df, meta = pyreadstat.read_sav('./data/umcg-copd-ashtma-dataset.sav')
df.describe()

# For SPSS sav files user defined missing values for non numeric (character) variables is not supported. In addition,
# if the value in a character variable is an empty string (''), it will not be translated to NaN, but will stay as an
# empty string. This is because the empty string is a valid character value in SPSS and pyreadstat preserves that
# property. You can convert empty strings to nan very easily with pandas.
df, meta = pyreadstat.read_sav('./data/umcg-copd-ashtma-dataset.sav', user_missing=True)  # preserve missing values as per original dataset (do not overwrite with NaN)
df.describe()

df, meta = pyreadstat.read_sav('./data/umcg-copd-ashtma-dataset.sav', user_missing=True, apply_value_formats=True)  # display value labels instead of raw values
df.describe()

print(df.head())

# print(meta.colum_names)  # AttributeError: 'metadata_container' object has no attribute 'colum_names'
print(meta.column_labels)
print(meta.number_rows)  # 19077
print(meta.number_columns)  # 2454
print(meta.file_label)  # none
print(meta.file_encoding)  # utf-8
# there are other metadata pieces extracted. See the documentation for more details.
